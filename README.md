TESTAR API - DECODER version
============================

### About

This api allows the remote execution of the TESTAR tool for testing a specific SUT, 
and the launchment in web service mode for the remote analysis of the inferred State Models and the visualization of the HTML test results.

The api allows the remote configuration of the TESTAR settings, but the specific SUT java protocol must be previously created.

### Compile TESTAR api and launch Spring Boot server

- Move to `testar_api`
- Execute `mvn install`
- This will create the TESTAR API file `testar_api\target\testar-client-api-1.X.0.jar`
- Place this api.jar inside `testar\bin\` directory
- From directory `testar_tool\bin\` execute `java -jar testar-client-api-1.X.0.jar`

The api will execute TESTAR via CLI, reading the output buffers to determine the success / erroneous execution 

### Execute TESTAR using a POST Execution

* Prepare a JSON file will all desired settings (see JSON Settings Example)

* Send a POST request with the settings as JSON data. You can use a reference to the JSON settings file

CURL execution example:
`` curl -X POST "http://127.0.0.1:8080/testar" -H "accept: application/json" -H "Content-Type: application/json" -d @settings_file.json ``

* The response will indicate you if TESTAR was correctly executed (and if the PKM Artefact was inserted), or if there is an error

### Launch Analysis mode for remote State Model visualization

* By indicating the OrientDB settings that contains the State Model, a web service will be executed by TESTAR.

* Access port 8090 and path models for review. Ex: `http://localhost:8090/models/models`

* Shutdown and stop service with shutdown path. Ex: `http://localhost:8090/models/shutdown`

### Launch Reports visualization mode

* By indicating the ArtefactID of the HTML report to review, a web service will be executed by TESTAR.

* Access port 8091 for review. Ex: `http://localhost:8091/`

* Shutdown and stop service with shutdown path. Ex: `http://localhost:8091/shutdown`


TESTAR Settings
---------------

* Settings that TESTAR needs to connect with the SUT, select the protocol, connect with the PKM and OrientDB are necessary.

### Most important settings

``sse : Select a specific TESTAR protocol to run (bin/settings/webdriver_MyThaiStar/Protocol_webdriver_MyThaiStar.java)``

``SUTConnector : Indicate how we want to connect to the SUT``

``SUTConnectorValue : Path to connect with the SUT``

``Sequences : Number of times to start the SUT, execute actions, stop the SUT and start again``

``SequenceLength : Number of actions for each sequence``

``OverrideWebDriverDisplayScale : (Default = 1.0) We recommend to configure the TESTAR environment with a 100% display (no zoom) but if is not possible, this setting Overrides the displayscale obtained from the system to solve problems when the mouse clicks are not aligned with the elements on the screen``

``PKMaddress : Web address that allows us to connect to the PKM-API``

``PKMport : Web port that listens request to the PKM-API``

``PKMdatabase : Project name of the PKM database on which we want to store the generated Artefacts``

``PKMusername : (Deprecated) We don't need the name, just the key``

``PKMkey : Key that identifies a user and hir permissions on PKM-API projects``

``StateModelEnabled : Generate a State Model``

``DataStoreType : How to connect with OrientDB (remote = ip address, plocal = local files)``

``DataStoreServer : IP address to connect with OrientDB``

``DataStoreDB : The name of the OrientDB database to store the State Model``

``DataStoreUser : User with permissions to read and write``

``DataStorePassword : Password credential``

``ResetDataStore : (WARNING) Delete all existing State Models from DataStoreDB before create a new one``

``ActionSelectionAlgorithm : Action selection mechanism to execute actions (Random, Unvisited)``

``StateModelStoreWidgets : Selects if we want to store all widgets for every State. This increases all used resources (storage, memory, time) but saves more information for State Model Difference HTML Report``

``AbstractStateAttributes : Specify the widget attributes that TESTAR uses to identify Abstract Widgets and States. Different attributes create different State Models``

``StateModelDifferenceAutomaticReport : Try to check if previous State Model exists to create the Model Difference HTML report``

``ApplicationName : Custom name to identify the SUT. Different names create different State Models``

``ApplicationVersion : Custom version to identify the SUT. Different versions create different State Models``

``PreviousApplicationName : Name that identifies the previous State Model``

``PreviousApplicationVersion : Version that identifies the previous State Model``

### SUTConnectorValue

``"SUTConnectorValue": " \"\"C:\\webdrivers\\chromedriver.exe\"\" \"\"http://10.101.0.243:8081\"\" "``

``"SUTConnectorValue": " \"/usr/bin/chromedriver\"\" \"http:/10.101.0.243:8081\" "``

### Other settings

* https://github.com/TESTARtool/TESTAR_dev/wiki/TESTAR-Settings

### JSON Settings Example:

		{
		  "sse": "webdriver_MyThaiStar",
		  "ShowVisualSettingsDialogOnStartup": false,
		  "Mode": "Generate",
		  "AccessBridgeEnabled": false,
		  "SUTConnector": "WEB_DRIVER",
		  "SUTConnectorValue": " \"\"C:\\webdrivers\\chromedriver.exe\"\" \"\"http:/10.101.0.243:8081\"\" ",
		  "Sequences": 1,
		  "SequenceLength": 50,
		  "SuspiciousTitles": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*",
		  "TimeToFreeze": 30,
		  "ProcessListenerEnabled": false,
		  "SuspiciousProcessOutput": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*",
		  "ProcessLogs": ".*[eE]rror.*|.*[eE]xcepti[o?]n.*",
		  "ClickFilter": ".*[sS]istema.*|.*[sS]ystem.*|.*[cC]errar.*|.*[cC]lose.*|.*[sS]alir.*|.*[eE]xit.*|.*[mM]inimizar.*|.*[mM]inimi[zs]e.*|.*[sS]ave.*|.*[gG]uardar.*|.*[fF]ormat.*|.*[fF]ormatear.*|.*[pP]rint.*|.*[iI]mprimir.*|.*[eE]mail.*|.*[fF]ile.*|.*[aA]rchivo.*|.*[dD]isconnect.*|.*[dD]esconectar.*",
		  "ForceForeground": true,
		  "ActionDuration": 0.5,
		  "TimeToWaitAfterAction": 0.5,
		  "MaxTime": 31536000,
		  "StartupTime": 5,
		  "MaxReward": 99999,
		  "Discount": 0.95,
		  "AlwaysCompile": true,
		  "OnlySaveFaultySequences": false,
		  "StopGenerationOnFault": false,
		  "ProcessesToKillDuringTest": "",
		  "PathToReplaySequence": "C:\\Users\\testar\\output\\sequences\\sequence1.testar",
		  "OutputDir": "output",
		  "TempDir": "output/temp",
		  "MyClassPath": "settings",
		  "OverrideWebDriverDisplayScale": 1.5,
		  "StateModelEnabled": true,
		  "DataStore": "OrientDB",
		  "DataStoreType": "remote",
		  "DataStoreServer": "10.102.0.244",
		  "DataStoreDirectory": "C:\\Users\\testar\\Desktop\\orientdb-3.0.34\\databases",
		  "DataStoreDB": "MyThaiStar",
		  "DataStoreUser": "testar",
		  "DataStorePassword": "testar",
		  "DataStoreMode": "instant",
		  "ResetDataStore": false,
		  "ActionSelectionAlgorithm": "unvisited",
		  "StateModelStoreWidgets": false,
		  "AbstractStateAttributes": "WebWidgetIsOffScreen,WebWidgetHref,WebWidgetTagName,WebWidgetTextContent",
		  "ApplicationName": "MyThaiStar",
		  "ApplicationVersion": 2,
		  "PreviousApplicationName": "MyThaiStar",
		  "PreviousApplicationVersion": 1,
		  "StateModelDifferenceAutomaticReport": true,
		  "PKMaddress": "10.101.0.224",
		  "PKMport": "8080",
		  "PKMdatabase": "myproject",
		  "PKMusername": "garfield",
		  "PKMkey": "5cCI6IkpXVCJ9.eyJ1c2VyX25hbWU"
		}
	
Swagger generated server
------------------------

Spring Boot Server 

## Overview  
This server was generated by the [swagger-codegen](https://github.com/swagger-api/swagger-codegen) project.  
By using the [OpenAPI-Spec](https://github.com/swagger-api/swagger-core), you can easily generate a server stub.  
This is an example of building a swagger-enabled server in Java using the SpringBoot framework.  

The underlying library integrating swagger to SpringBoot is [springfox](https://github.com/springfox/springfox)  

Start your server as an simple java application  

Change default port value in application.properties
