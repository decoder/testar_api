/**
 * NOTE: This class is auto generated by the swagger code generator program (3.0.20).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package io.swagger.api;

import io.swagger.model.Settings;
import io.swagger.model.TestarResponse;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.validation.Valid;
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-11-06T10:40:54.389+01:00[Europe/Paris]")
@Api(value = "testar", description = "the testar API")
public interface TestarApi {

	// TESTAR POST Execution to Generate TestResults and State Model
	@ApiOperation(value = "", nickname = "testarPost", notes = "", response = TestarResponse.class, tags={ "TESTAR", })
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "TESTAR was executed correctly, obtain the Artefacts Id.", response = TestarResponse.class),
	        @ApiResponse(code = 400, message = "Invalid TESTAR settings request", response = String.class),
	        @ApiResponse(code = 401, message = "ERROR with OrientDB connection values or credentials", response = String.class),
	        @ApiResponse(code = 404, message = "ERROR with the creation of the Artefacts", response = String.class) })
	@RequestMapping(value = "/testar",
	produces = { "application/json" }, 
	consumes = { "application/json" },
	method = RequestMethod.POST)
	ResponseEntity<Object> testarPost(@ApiParam(value = "Send the desired parameters to launch TESTAR through command line, connect with the SUT, generate the Artefacts and feed the PKM using the PKM-API with authentication." ,required=true )  @Valid @RequestBody Settings body
			);

	// TESTAR GET TestResults Report (launch TESTAR as a remote web server to visualize output files)
	@ApiOperation(value = "", nickname = "testarReportArtefactIdGet", notes = "Visualize the HTML sequence report from the artefactID with the specific sequence number", tags={ "TESTAR", })
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "TESTAR HTML report web server loaded. Visualize the reports using port 8091 (10.X.X.X:8091). Shutdown the server with the path /shutdown (10.X.X.X:8091/shutdown)."),
			@ApiResponse(code = 400, message = "Invalid request"),
			@ApiResponse(code = 404, message = "ERROR trying to find TESTAR HTML report") })
	@RequestMapping(value = "/testar/report/{artefactId}",
	method = RequestMethod.GET)
	ResponseEntity<Void> testarReportArtefactIdGet(@ApiParam(value = "Indicate the ArtefactId of the Test Results reports.",required=true) @PathVariable("artefactId") String artefactId
			);

	// TESTAR GET StateModel Analysis (launch TESTAR as a remote web server to analyze models)
	@ApiOperation(value = "", nickname = "testarAnalysisOrientDBnameGet", notes = "Launch TESTAR Analysis mode to acces to the OrientDB database and visualize the State Model.", tags={ "TESTAR", })
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "TESTAR Analysis web server loaded, access using port 8090 and path /models. Shutdown the server with the path /shutdown."),
			@ApiResponse(code = 400, message = "Invalid request"),
			@ApiResponse(code = 401, message = "ERROR with OrientDB or PKM credentials", response = String.class),
			@ApiResponse(code = 404, message = "ERROR trying to load Analysis web server") })
	@RequestMapping(value = "/testar/analysis/{OrientDBname}",
	method = RequestMethod.GET)
	ResponseEntity<Void> testarAnalysisOrientDBnameGet(@ApiParam(value = "Indicate the OrientDB database name.",required=true) @PathVariable("OrientDBname") String orientDBname
			,@ApiParam(value = "Access key to the PKM" ,required=true) @RequestHeader(value="key", required=true) String key
			);
}
