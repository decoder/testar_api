package io.swagger.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2020-11-06T10:40:54.389+01:00[Europe/Paris]")
@Configuration
public class SwaggerDocumentationConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("DECODER - TESTAR tool invocation API")
            .description("An API that allows the integration of the TESTAR tool in the DECODER H2020 EU project to execute and generate Artefacts. This API will run on the TESTAR client machine.")
            .license("BSD 3-Clause")
            .licenseUrl("https://github.com/TESTARtool/TESTAR_dev/blob/master/LICENSE")
            .termsOfServiceUrl("https://testar.org/")
            .version("1.7.0")
            .contact(new Contact("","", "fernando@testar.org"))
            .build();
    }

    @Bean
    public Docket customImplementation(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                    .apis(RequestHandlerSelectors.basePackage("io.swagger.api"))
                    .build()
                .directModelSubstitute(org.threeten.bp.LocalDate.class, java.sql.Date.class)
                .directModelSubstitute(org.threeten.bp.OffsetDateTime.class, java.util.Date.class)
                .apiInfo(apiInfo());
    }

}
