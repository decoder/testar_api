/***************************************************************************************************
 *
 * Copyright (c) 2020 Universitat Politecnica de Valencia - www.upv.es
 * Copyright (c) 2020 Open Universiteit - www.ou.nl
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************************************/

package io.swagger.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HtmlReportService implements IHtmlReportService {

	private static final Logger log = LoggerFactory.getLogger(HtmlReportService.class);

	@Override
	public Void htmlReport(String artefactId) throws Exception {

		List<String> commands = new ArrayList<>();
		commands.add("testar.bat");

		// TESTAR Report Mode is used to start the web service that allows remote HTML visualization
		// testar ShowVisualSettingsDialogOnStartup=false Mode=Report HTMLreportServerFile=5f64ba3bf87fe8088421ba7e
		commands.add("Mode=Report");

		// Disable TESTAR GUI
		commands.add("ShowVisualSettingsDialogOnStartup=false");

		// Specify the ArtefactId of the HTML report
		commands.add("HTMLreportServerFile=" + artefactId);

		String[] commadsArray = commands.toArray(new String[0]);
		StringBuilder cmdToExecute = new StringBuilder();

		for (String string : commadsArray) {
			log.info(string);
			cmdToExecute.append(string + " ");
		}

		ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", cmdToExecute.toString());
		Process p = builder.start();

		// Wait until user launchs "ipaddress:8091/shutdown" URL
		p.waitFor();

		return null;
	}

}
